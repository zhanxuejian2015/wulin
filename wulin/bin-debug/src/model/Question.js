/**
 * Created by bannika on 15/6/3.
 */
var Question = (function () {
    function Question() {
    }

    var __egretProto__ = Question.prototype;
    Object.defineProperty(__egretProto__, "w", {
        get: function () {
            return this._w;
        },
        set: function (value) {
            this._w = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(__egretProto__, "a", {
        get: function () {
            return this._a;
        },
        set: function (value) {
            this._a = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(__egretProto__, "a1", {
        get: function () {
            return this._a1;
        },
        set: function (value) {
            this._a1 = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(__egretProto__, "a2", {
        get: function () {
            return this._a2;
        },
        set: function (value) {
            this._a2 = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(__egretProto__, "a3", {
        get: function () {
            return this._a3;
        },
        set: function (value) {
            this._a3 = value;
        },
        enumerable: true,
        configurable: true
    });
    Object.defineProperty(__egretProto__, "a4", {
        get: function () {
            return this._a4;
        },
        set: function (value) {
            this._a4 = value;
        },
        enumerable: true,
        configurable: true
    });
    return Question;
})();
Question.prototype.__class__ = "Question";
