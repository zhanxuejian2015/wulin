var CommonTextInput = (function (_super) {
    __extends(CommonTextInput, _super);
    /**
     * 构造函数
     */
    function CommonTextInput() {
        _super.call(this);
        this.multiline = false;
        this.numLines = 1;
        this.textAlign = "center";
        this.textColor = 0X000000;
        this.addEventListener(egret.Event.ADDED_TO_STAGE, this.createView, this);
    }

    var __egretProto__ = CommonTextInput.prototype;
    __egretProto__.createView = function () {
        var bg = new egret.Bitmap();
        bg.texture = RES.getRes("textInput");
        this.textinput = new egret.TextField();
        this.textinput.text = this.text;
        this.textinput.height = 40;
        this.textinput.width = 250;
        this.textinput.setFocus();
        this.textinput.maxChars = this.maxchar;
        bg.width = this.textinput.width + 10;
        bg.height = this.textinput.height + 10;
        this.textinput.multiline = this.multiline;
        this.textinput.numLines = this.numLines;
        this.textinput.textAlign = this.textAlign;
        this.textinput.textColor = this.textColor;
        this.textinput.type = egret.TextFieldType.INPUT;
        this.textinput.x = bg.width / 2 - this.textinput.width / 2;
        this.textinput.y = bg.height / 2 - this.textinput.height / 2;
        this.addChild(bg);
        this.addChild(this.textinput);
    };
    __egretProto__.getText = function () {
        return this.textinput.text;
    };
    return CommonTextInput;
})(egret.DisplayObjectContainer);
CommonTextInput.prototype.__class__ = "CommonTextInput";
