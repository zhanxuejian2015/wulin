var LoginScene = (function (_super) {
    __extends(LoginScene, _super);
    function LoginScene(main) {
        _super.call(this);
        this.main = main;
        this.addEventListener(egret.Event.ADDED_TO_STAGE, this.onAddToStage, this);
    }

    var __egretProto__ = LoginScene.prototype;
    __egretProto__.onAddToStage = function () {
        var panel = new CommonPanel();
        panel.title = "输入你的手机";
        this.addChild(panel);
        this.txtinput = new CommonTextInput();
        this.txtinput.maxchar = 11;
        this.txtinput.x = panel.getCenterX(250);
        this.txtinput.y = 90;
        this.txtTip = new egret.TextField();
        this.txtTip.textColor = 0X999999;
        this.txtTip.size = 20;
        this.txtTip.x = this.txtinput.x;
        this.txtTip.y = this.txtinput.y + 70;
        this.submitBtn = new CommonButton();
        this.submitBtn.texture = "common.play";
        this.submitBtn.x = panel.getCenterX(113);
        this.submitBtn.y = this.txtTip.y + 100;
        panel.content.addChild(this.submitBtn);
        panel.content.addChild(this.txtinput);
        panel.content.addChild(this.txtTip);
        this.submitBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onSubmitButtonClick, this);
    };
    __egretProto__.onSubmitButtonClick = function (event) {
        var phonenum = this.txtinput.getText();
        if (phonenum != null && phonenum.length == 11 && phonenum.match("^[1][3-8]\\d{9}$") != null && phonenum.match("^[1][3-8]\\d{9}$").length > 0) {
            this.main.uid = phonenum;
            var isUserInfoNeeded = this.getUserInfoNeeded(this.main.score);
            if (isUserInfoNeeded) {
            }
            var ss = new ShareScene(this.main);
            this.main.removeChildren();
            this.main.addChild(ss);
        }
        else {
            this.txtTip.text = "手机号码不正确，请重新输入";
        }
    };
    /**
     * 上传分数
     */
    __egretProto__.uploadScore = function () {
        var url = this.main.appurl + "a?id=" + this.main.uid;
        var loader = new egret.URLLoader();
        loader.dataFormat = egret.URLLoaderDataFormat.TEXT;
        loader.addEventListener(egret.Event.COMPLETE, this.onGetComplete, this);
        loader.load(new egret.URLRequest(url));
    };
    __egretProto__.onGetComplete = function (event) {
        var loader = event.target;
        var data = loader.data;
        //创建POST请求
        var url = this.main.appurl + "record/s";
        var loader = new egret.URLLoader();
        loader.dataFormat = egret.URLLoaderDataFormat.VARIABLES;
        loader.addEventListener(egret.Event.COMPLETE, this.onPostComplete, this);
        var request = new egret.URLRequest(url);
        request.method = egret.URLRequestMethod.POST;
        request.data = new egret.URLVariables("t=" + data.toString() + "&appid=" + this.main.appid + "&es=" + window.btoa(this.main.score.toString()) + "&u=" + this.main.uid);
        loader.load(request);
    };
    __egretProto__.onPostComplete = function (event) {
        var loader = event.target;
        var data = loader.data;
    };
    /**
     *需要输入用户信息么？
     * @param score
     * @returns {boolean}
     */
    __egretProto__.getUserInfoNeeded = function (score) {
        if (score >= 3) {
            return true;
        }
        else {
            return false;
        }
    };
    return LoginScene;
})(egret.DisplayObjectContainer);
LoginScene.prototype.__class__ = "LoginScene";
