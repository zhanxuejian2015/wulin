var ResultScene = (function (_super) {
    __extends(ResultScene, _super);
    function ResultScene(main) {
        _super.call(this);
        this.main = main;
        this.addEventListener(egret.Event.ADDED_TO_STAGE, this.createScene, this);
    }

    var __egretProto__ = ResultScene.prototype;
    __egretProto__.createScene = function () {
        var panel = new CommonPanel();
        panel.title = "你获得了" + this.main.score + "分", this.restartBtn = new CommonButton();
        this.restartBtn.label = "再玩一次";
        this.restartBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onReStartButtonClick, this);
        this.shareBtn = new CommonButton();
        this.shareBtn.label = "炫耀一下";
        this.shareBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onShareButtonClick, this);
        this.addChild(panel);
        this.restartBtn.x = panel.getCenterX(214);
        this.restartBtn.y = 60;
        this.shareBtn.x = panel.getCenterX(214);
        this.shareBtn.y = this.restartBtn.y + 80;
        panel.content.addChild(this.restartBtn);
        panel.content.addChild(this.shareBtn);
    };
    __egretProto__.onReStartButtonClick = function (event) {
        var gs = new GameScene(this.main);
        gs.initQuestion();
        this.main.removeChildren();
        this.main.addChild(gs);
    };
    __egretProto__.onShareButtonClick = function (event) {
        var login = new LoginScene(this.main);
        this.main.removeChildren();
        this.main.addChild(login);
    };
    return ResultScene;
})(egret.DisplayObjectContainer);
ResultScene.prototype.__class__ = "ResultScene";
