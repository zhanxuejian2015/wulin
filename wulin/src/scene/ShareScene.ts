class ShareScene extends egret.DisplayObjectContainer {

    public main:Main;
    public replayBtn:CommonButton;

    public constructor(main:Main) {
        super();
        this.main = main;
        this.addEventListener(egret.Event.ADDED_TO_STAGE, this.onAddToStage, this);
    }

    public onAddToStage():void {
        this.touchEnabled = true;
        document.title = "你真的知道武林群侠传么？我得了" + this.main.score + "分";
        var sharepic:egret.Bitmap = new egret.Bitmap();
        sharepic.width = 480;
        sharepic.height = 180;
        sharepic.texture = RES.getRes("common.share");
        this.replayBtn = new CommonButton();
        this.replayBtn.x = 240 - 35;
        this.replayBtn.y = sharepic.y + sharepic.height + 5;

        this.replayBtn.texture = "common.replay";
        this.replayBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onTapClick, this);
        this.addChild(sharepic);
        this.addChild(this.replayBtn);
    }

    private onTapClick(event:egret.TouchEvent):void {
        var gs:GameScene =
            new GameScene(this.main);
        gs.initQuestion();
        this.main.removeChildren();
        this.main.addChild(gs);
    }


}