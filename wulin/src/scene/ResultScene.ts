class ResultScene extends egret.DisplayObjectContainer {
    public main:Main;

    public constructor(main:Main) {
        super();
        this.main = main;
        this.addEventListener(egret.Event.ADDED_TO_STAGE, this.createScene, this);
    }

    public restartBtn:CommonButton;
    public shareBtn:CommonButton;

    public createScene():void {
        var panel:CommonPanel = new CommonPanel();
        panel.title = "你获得了" + this.main.score + "分",
            this.restartBtn = new CommonButton();
        this.restartBtn.label = "再玩一次";
        this.restartBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onReStartButtonClick, this);
        this.shareBtn = new CommonButton();
        this.shareBtn.label = "炫耀一下";
        this.shareBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onShareButtonClick, this);

        this.addChild(panel);
        this.restartBtn.x = panel.getCenterX(214);
        this.restartBtn.y = 60;

        this.shareBtn.x = panel.getCenterX(214);
        this.shareBtn.y = this.restartBtn.y + 80;

        panel.content.addChild(this.restartBtn);
        panel.content.addChild(this.shareBtn);

    }

    private onReStartButtonClick(event:egret.TouchEvent):void {
        var gs:GameScene = new GameScene(this.main);
        gs.initQuestion();
        this.main.removeChildren();
        this.main.addChild(gs);
    }

    private onShareButtonClick(event:egret.TouchEvent):void {

        var login:LoginScene = new LoginScene(this.main);
        this.main.removeChildren();
        this.main.addChild(login);

    }

}