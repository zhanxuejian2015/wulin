/**
 * Created by Administrator on 2015/5/15 0015.
 */

class GameScene extends egret.DisplayObjectContainer {
    public main:Main;

    public constructor(main:Main) {
        super();
        this.main = main;
        this.initQuestion();
        this.addEventListener(egret.Event.ADDED_TO_STAGE, this.createScene, this);
    }


    private score:number = 0;
    private rightAnswer:number = 0;
    private score_label:egret.TextField;
    private wenti_label:egret.TextField;
    private xx1_label:egret.TextField;
    private xx2_label:egret.TextField;
    private xx3_label:egret.TextField;
    private xx4_label:egret.TextField;
    private duihuakuang:egret.Bitmap;
    private questionList:Array<Question>;
    public count:number = 0;
    /**
     * 0:没有回答
     * 1：回答错误
     * 2：回答正确
     */
    private state:number = 0;


    private  xiaoxiami:egret.Bitmap;

    private bgm:egret.Sound;

    /**
     * 创建场景界面
     * Create scene interface
     */
    private createScene():void {
        RES.addEventListener(RES.ResourceEvent.GROUP_COMPLETE, this.onResourceLoadComplete, this);
        RES.loadGroup("soundload");
        //游戏场景层，游戏场景相关内容可以放在这里面。
        var bg:egret.Bitmap = new egret.Bitmap();
        bg.texture = RES.getRes("bgImage");
        bg.width = 480;
        bg.height = 800;
        this.addChild(bg);


        //小虾米
        this.xiaoxiami = new egret.Bitmap();
        this.xiaoxiami.texture = RES.getRes("tiwen");
        this.xiaoxiami.width = 480;
        this.xiaoxiami.height = 560;
        this.xiaoxiami.y = 0;
        this.addChild(this.xiaoxiami);

        //对话框
        this.duihuakuang = new egret.Bitmap();
        this.duihuakuang.texture = RES.getRes("duihuakuang");
        this.duihuakuang.width = 480;
        this.duihuakuang.height = 300;
        this.duihuakuang.y = 500;
        this.addChild(this.duihuakuang);
        this.duihuakuang.touchEnabled = true;
        this.duihuakuang.addEventListener(egret.TouchEvent.TOUCH_TAP, this.getNextQuestion, this);

        //问题
        this.wenti_label = new egret.TextField();
        this.wenti_label.textColor = 0x000000;
        this.wenti_label.width = 450;
        this.wenti_label.text = "新武林要发布了，小虾米有几个问题要考考大家~";
        this.wenti_label.x = this.duihuakuang.x + 20;
        this.wenti_label.y = this.duihuakuang.y + 40;
        this.addChild(this.wenti_label);
        //选项一
        this.xx1_label = new egret.TextField();
        this.xx1_label.textColor = 0x000000;
        this.xx1_label.maxWidth = 460;
        this.xx1_label.numLines = 1;
        //this.xx1_label.text ="asdasdasd456465";

        this.addChild(this.xx1_label);
        this.xx1_label.addEventListener(egret.TouchEvent.TOUCH_TAP, this.choseA, this);
        //选项二
        this.xx2_label = new egret.TextField();
        this.xx2_label.textColor = 0x000000;
        this.xx2_label.width = 460;
        this.xx2_label.numLines = 1;
        //this.xx2_label.text = "asdasdasda1212121";
        this.xx2_label.x = this.duihuakuang.x + 20;
        this.xx2_label.y = this.xx1_label.y + this.xx1_label.height + 10;
        this.addChild(this.xx2_label);
        this.xx2_label.addEventListener(egret.TouchEvent.TOUCH_TAP, this.choseB, this);
        //选项三
        this.xx3_label = new egret.TextField();
        this.xx3_label.textColor = 0x000000;
        this.xx3_label.width = 460;
        this.xx3_label.numLines = 1;
        //this.xx3_label.text = "123123123123asda";
        this.xx3_label.x = this.duihuakuang.x + 20;
        this.xx3_label.y = this.xx2_label.y + this.xx2_label.height + 10;
        this.addChild(this.xx3_label);
        this.xx3_label.addEventListener(egret.TouchEvent.TOUCH_TAP, this.choseC, this);
        // 选项四
        this.xx4_label = new egret.TextField();
        this.xx4_label.textColor = 0x000000;
        this.xx4_label.width = 460;
        this.xx4_label.numLines = 1;
        //this.xx4_label.text = "jkhs dsahjd hkasjhd kajsh dkjash dasdasfadsdfasfli jkljas fdlkaj flk";
        this.xx4_label.x = this.duihuakuang.x + 20;
        this.xx4_label.y = this.xx3_label.y + this.xx3_label.height + 10;
        this.addChild(this.xx4_label);
        this.xx4_label.addEventListener(egret.TouchEvent.TOUCH_TAP, this.choseD, this);


        //分数
        this.score_label = new egret.TextField();
        this.score_label.size = 50;
        this.score_label.stroke = 2;
        this.score_label.strokeColor = 0xffffff;
        this.score_label.textColor = 0x555555;
        this.score_label.text = this.score + "分";
        this.score_label.x = 440 - this.score_label.width;
        this.score_label.y = 20;

        this.addChild(this.score_label);

    }


    /**
     * 资源组加载完成
     * Preload resource group is loaded
     */
    private onResourceLoadComplete(event:RES.ResourceEvent):void {
        if (event.groupName == "soundload") {
            this.bgm = RES.getRes("bgm");
            this.bgm.stop();
            this.bgm.play(true);
        }
    }

    /**
     * 初始化问题
     */

    public  initQuestion():boolean {
        this.questionList = RES.getRes("qs");
        return true;
    }

    /**
     * 关闭音乐
     * @returns {boolean}
     */
    public  stopMusic():boolean {
        this.bgm.stop();
        return true;
    }


    private  getNextQuestion():void {
        this.changeXiaoxiami();

    }

    /**
     *小虾米表情变化
     */
    private  changeXiaoxiami():void {

        if (this.state == 0) {
            this.xiaoxiami.texture = RES.getRes("tiwen");
            this.getQuestion();
        } else if (this.state == 1) {
            this.xiaoxiami.texture = RES.getRes("cuole");
            this.wenti_label.text = "你回答错了！！！";
            this.state = 0;
            this.reset();
        } else if (this.state == 2) {
            this.xiaoxiami.texture = RES.getRes("dakeshui");
            this.wenti_label.text = "你回答对了！！！";
            this.score += 10;
            this.score_label.text = this.score + "分";
            this.reset();

        }

    }


    private  reset():void {
        this.rightAnswer = 0;
        this.state = 0;
        this.xx1_label.text = "";
        this.xx2_label.text = "";
        this.xx3_label.text = "";
        this.xx4_label.text = "";
        this.xx1_label.touchEnabled = false;
        this.xx2_label.touchEnabled = false;
        this.xx3_label.touchEnabled = false;
        this.xx4_label.touchEnabled = false;
        this.duihuakuang.touchEnabled = true;
    }

    /**
     * 获取问题
     */

    private  getQuestion():void {
        if (this.count < this.questionList.length) {
            var q = this.questionList[this.count];
            this.wenti_label.text = q.w;
            this.xx1_label.text = q.a1;
            this.xx2_label.text = q.a2;
            this.xx3_label.text = q.a3;
            this.xx4_label.text = q.a4;
            this.rightAnswer = q.a;
            this.duihuakuang.touchEnabled = false;
            this.xx1_label.touchEnabled = true;
            this.xx2_label.touchEnabled = true;
            this.xx3_label.touchEnabled = true;
            this.xx4_label.touchEnabled = true;
            this.xx1_label.x = this.duihuakuang.x + 20;
            this.xx1_label.y = this.wenti_label.y + this.wenti_label.height + 20;
            this.xx2_label.x = this.duihuakuang.x + 20;
            this.xx2_label.y = this.xx1_label.y + this.xx1_label.height + 20;
            this.xx3_label.x = this.duihuakuang.x + 20;
            this.xx3_label.y = this.xx2_label.y + this.xx2_label.height + 20;
            this.xx4_label.x = this.duihuakuang.x + 20;
            this.xx4_label.y = this.xx3_label.y + this.xx3_label.height + 20;
            this.count++;
        } else {
            this.gameover();
        }


    }


    /**
     *游戏结束
     */

    private gameover():void {
        this.main.score = this.score;

        var resultScene:ResultScene = new ResultScene(this.main);
        this.main.removeChildren();
        this.main.addChild(resultScene);

    }

    /**
     *选择A
     */

    private choseA(even:egret.TouchEvent):void {


        if (this.rightAnswer == 1) {
            this.state = 2;
        } else {
            this.state = 1;
        }

        this.changeXiaoxiami();

    }

    /**
     *选择B
     */

    private choseB(even:egret.TouchEvent):void {

        if (this.rightAnswer == 2) {
            this.state = 2;
        } else {
            this.state = 1;
        }

        this.changeXiaoxiami();
    }

    /**
     *选择C
     */

    private choseC(even:egret.TouchEvent):void {
        if (this.rightAnswer == 3) {
            this.state = 2;
        } else {
            this.state = 1;
        }


        this.changeXiaoxiami();
    }

    /**
     *选择D
     */

    private choseD(even:egret.TouchEvent):void {
        if (this.rightAnswer == 4) {
            this.state = 2;
        } else {
            this.state = 1;
        }

        this.changeXiaoxiami();

    }


}
