class LoginScene extends egret.DisplayObjectContainer {

    public main:Main;
    public submitBtn:CommonButton;
    public txtinput:CommonTextInput;
    public txtTip:egret.TextField;

    public constructor(main:Main) {
        super();
        this.main = main;
        this.addEventListener(egret.Event.ADDED_TO_STAGE, this.onAddToStage, this);
    }

    public onAddToStage():void {
        var panel:CommonPanel = new CommonPanel();
        panel.title = "输入你的手机";
        this.addChild(panel);
        this.txtinput = new CommonTextInput();
        this.txtinput.maxchar = 11;
        this.txtinput.x = panel.getCenterX(250);
        this.txtinput.y = 90;
        this.txtTip = new egret.TextField();
        this.txtTip.textColor = 0X999999;
        this.txtTip.size = 20;
        this.txtTip.x = this.txtinput.x;
        this.txtTip.y = this.txtinput.y + 70;

        this.submitBtn = new CommonButton();
        this.submitBtn.texture = "common.play";
        this.submitBtn.x = panel.getCenterX(113);
        this.submitBtn.y = this.txtTip.y + 100;
        panel.content.addChild(this.submitBtn);
        panel.content.addChild(this.txtinput);
        panel.content.addChild(this.txtTip);
        this.submitBtn.addEventListener(egret.TouchEvent.TOUCH_TAP, this.onSubmitButtonClick, this);

    }

    private onSubmitButtonClick(event:egret.TouchEvent):void {
        var phonenum:string = this.txtinput.getText();
        if (phonenum != null && phonenum.length == 11 && phonenum.match("^[1][3-8]\\d{9}$") != null && phonenum.match("^[1][3-8]\\d{9}$").length > 0) {
            this.main.uid = phonenum;
            var isUserInfoNeeded:boolean = this.getUserInfoNeeded(this.main.score);
            if (isUserInfoNeeded) {
                //this.uploadScore();
            }
            var ss:ShareScene = new ShareScene(this.main);
            this.main.removeChildren();
            this.main.addChild(ss);
        } else {
            this.txtTip.text = "手机号码不正确，请重新输入"
        }
    }

    /**
     * 上传分数
     */
    private uploadScore():void {
        var url:string = this.main.appurl + "a?id=" + this.main.uid;
        var loader:egret.URLLoader = new egret.URLLoader();
        loader.dataFormat = egret.URLLoaderDataFormat.TEXT;
        loader.addEventListener(egret.Event.COMPLETE, this.onGetComplete, this);
        loader.load(new egret.URLRequest(url));
    }

    private onGetComplete(event:egret.Event):void {
        var loader:egret.URLLoader = <egret.URLLoader> event.target;
        var data:egret.URLVariables = loader.data;
        //创建POST请求
        var url:string = this.main.appurl + "record/s";
        var loader:egret.URLLoader = new egret.URLLoader();
        loader.dataFormat = egret.URLLoaderDataFormat.VARIABLES;
        loader.addEventListener(egret.Event.COMPLETE, this.onPostComplete, this);
        var request:egret.URLRequest = new egret.URLRequest(url);
        request.method = egret.URLRequestMethod.POST;
        request.data = new egret.URLVariables("t=" + data.toString() + "&appid=" + this.main.appid + "&es=" + window.btoa(this.main.score.toString()) + "&u=" + this.main.uid);
        loader.load(request);
    }

    private onPostComplete(event:egret.Event):void {
        var loader:egret.URLLoader = <egret.URLLoader> event.target;
        var data:egret.URLVariables = loader.data;

    }


    /**
     *需要输入用户信息么？
     * @param score
     * @returns {boolean}
     */
    public  getUserInfoNeeded(score:number):boolean {
        if (score >= 3) {
            return true;
        } else {
            return false;
        }
    }
}