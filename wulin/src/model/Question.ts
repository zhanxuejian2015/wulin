/**
 * Created by bannika on 15/6/3.
 */
class Question {

    private _w:string;
    private _a:number;
    private _a1:string;
    private _a2:string;
    private _a3:string;
    private _a4:string;


    public get w():string {
        return this._w;
    }

    public set w(value:string) {
        this._w = value;
    }

    public get a():number {
        return this._a;
    }

    public set a(value:number) {
        this._a = value;
    }

    public get a1():string {
        return this._a1;
    }

    public set a1(value:string) {
        this._a1 = value;
    }

    public get a2():string {
        return this._a2;
    }

    public set a2(value:string) {
        this._a2 = value;
    }

    public get a3():string {
        return this._a3;
    }

    public set a3(value:string) {
        this._a3 = value;
    }

    public get a4():string {
        return this._a4;
    }

    public set a4(value:string) {
        this._a4 = value;
    }
}