/**
 * Created by bannika on 15/5/27.
 */
class CommonPanel extends egret.DisplayObjectContainer {

    public constructor() {
        super();
        this.addEventListener(egret.Event.ADDED_TO_STAGE, this.createView, this);

    }

    public texture:string = "common.panel";
    public texture_panel:string = "common.paneltitle";
    public title:string;
    public panelWidth:number = 450;
    public panelHeight:number = 440;

    public content:egret.DisplayObjectContainer;

    public getCenterX(spriteWidth:number):number {

        return (450 - spriteWidth) / 2 - this.x - 10;

    }

    private createView() {
        var img:egret.Bitmap = new egret.Bitmap();
        img.texture = RES.getRes(this.texture);
        img.width = this.panelWidth;
        img.height = this.panelHeight;
        img.x = 480 / 2 - this.panelWidth / 2;
        img.y = 600 / 2 - this.panelHeight / 2;
        var img_title:egret.Bitmap = new egret.Bitmap();
        img_title.texture = RES.getRes(this.texture_panel);
        img_title.x = 480 / 2 - img_title.width / 2;
        img_title.y = img.y - img_title.height / 2 + 20;
        var labelTF:egret.TextField = new egret.TextField();
        labelTF.text = this.title;
        labelTF.x = img_title.width / 2 + img_title.x - labelTF.width / 2;
        labelTF.y = img_title.y + labelTF.height / 2 + 5;
        this.content = new egret.DisplayObjectContainer();
        this.content.width = 405;
        this.content.height = 290;
        this.content.x = img.x + 20;
        this.content.y = img.y + img_title.height / 2;
        this.addChild(img);
        this.addChild(img_title);
        this.addChild(labelTF);
        this.addChild(this.content);
        this.touchEnabled = true;
    }

}