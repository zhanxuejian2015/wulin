/**
 * Created by bannika on 15/5/27.
 */
class CommonButton extends egret.DisplayObjectContainer {

    public constructor() {
        super();
        this.addEventListener(egret.Event.ADDED_TO_STAGE, this.createView, this);

    }

    public texture:string = "common.yellowbtn";
    public label:string;


    private createView() {
        var img:egret.Bitmap = new egret.Bitmap();
        img.texture = RES.getRes(this.texture);
        var labelTF:egret.TextField = new egret.TextField();
        labelTF.text = this.label;
        labelTF.x = img.width / 2 - labelTF.width / 2;
        labelTF.y = img.height / 2 - labelTF.height / 2;
        this.addChild(img);
        this.addChild(labelTF);
        this.touchEnabled = true;
    }


}