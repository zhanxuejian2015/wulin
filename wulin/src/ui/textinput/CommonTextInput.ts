class CommonTextInput extends egret.DisplayObjectContainer {
    /**
     * 构造函数
     */
    public constructor() {
        super();
        this.addEventListener(egret.Event.ADDED_TO_STAGE, this.createView, this);
    }

    /**
     * 初始化text
     */
    public  text:string;
    public  maxchar:number;
    public  multiline:boolean = false;
    public  numLines:number = 1;
    public textAlign:string = "center";
    public  textColor:number = 0X000000;
    private textinput:egret.TextField;

    private createView() {

        var bg:egret.Bitmap = new egret.Bitmap();
        bg.texture = RES.getRes("textInput");
        this.textinput = new egret.TextField();

        this.textinput.text = this.text;
        this.textinput.height = 40;
        this.textinput.width = 250;
        this.textinput.setFocus();
        this.textinput.maxChars = this.maxchar;
        bg.width = this.textinput.width + 10;
        bg.height = this.textinput.height + 10;
        this.textinput.multiline = this.multiline;
        this.textinput.numLines = this.numLines;
        this.textinput.textAlign = this.textAlign;
        this.textinput.textColor = this.textColor;
        this.textinput.type = egret.TextFieldType.INPUT;
        this.textinput.x = bg.width / 2 - this.textinput.width / 2;
        this.textinput.y = bg.height / 2 - this.textinput.height / 2;
        this.addChild(bg);
        this.addChild(this.textinput);


    }

    public getText():string {
        return this.textinput.text;
    }


}